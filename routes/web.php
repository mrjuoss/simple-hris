<?php

use App\Http\Controllers\Backend\ChangePasswordController;
use App\Http\Controllers\Backend\CityController;
use App\Http\Controllers\Backend\CountryController;
use App\Http\Controllers\Backend\DepartmentController;
use App\Http\Controllers\Backend\StateController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guest()) {
        return view('auth.login');
    } else {
        return view('home');
    }
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::resource('users', UserController::class)->middleware('auth');
Route::resource('countries', CountryController::class)->middleware('auth');
Route::resource('states', StateController::class)->middleware('auth');
Route::resource('cities', CityController::class)->middleware('auth');
Route::resource('departments', DepartmentController::class);

Route::get('/users/{user}/change-pwd', [ChangePasswordController::class, 'index'])->name('users.change.pwd');
Route::post('/users/{user}/change-password', [ChangePasswordController::class, 'change_password'])->name('users.change.password');

Route::get('{any}', function () {
    return view('employees.index');
})->where('any', '.*');
