@extends('layouts.main')

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Cities</h1>
</div>
<div class="row">
  <div class="card mx-auto">
        <div>
            @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session('success_message') }}
                </div>
            @endif
        </div>
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col">
                    <form method="GET" action="{{ route('cities.index') }}">
                        <div class="form-row align-items-center">
                            <div class="col">
                                <input type="search" name="search" class="form-control" placeholder="state name"/>
                            </div>
                            <div class="col">
                                <input type="submit" value="Search" class="btn btn-success btn-sm" />
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col">
                    <a href="{{ route('cities.create') }}" class="btn btn-success btn-sm float-right">Create</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">State Name</th>
                        <th scope="col">City Name</th>
                        <th scope="col" colspan="2">Manage</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($cities as $city)

                  <tr>
                      <th scope="row">{{ $loop->iteration}}</th>
                      <td>{{ $city->state->name }}</td>
                      <td>{{ $city->name }}</td>
                      <td>
                          <a href="{{ route('cities.edit', $city->id) }}" class="btn btn-success btn-sm">Edit</a>
                      </td>
                      <td>
                          <form method="POST" action="{{ route('cities.destroy', $city->id) }}">
                              @csrf
                              @method('DELETE')
                              <button class="btn btn-danger btn-sm">Delete</button>
                          </form>
                      </td>
                  </tr>

                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
