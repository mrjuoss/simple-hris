@include('layouts.partials.header')
@include('layouts.partials.sidebar')
<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
        <!-- Topbar -->
        @include('layouts.partials.navbar')
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Content Row -->
            @yield('content')
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
@include('layouts.partials.footer')
