<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="/">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class="nav-link" href="/employees">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Employee Management</span></a>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSystemManagement"
            aria-expanded="true" aria-controls="collapseSystemManagement">
            <i class="fas fa-fw fa-cog"></i>
            <span>System Management</span>
        </a>
        <div id="collapseSystemManagement" class="collapse" aria-labelledby="headingTwo"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('countries.index') }}">Countries</a>
                <a class="collapse-item" href="{{ route('states.index') }}">States</a>
                <a class="collapse-item" href="{{ route('cities.index') }}">Cities</a>
                <a class="collapse-item" href="{{ route('departments.index') }}">Department</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsersManagement"
            aria-expanded="true" aria-controls="collapseUsersManagement">
            <i class="fas fa-fw fa-cog"></i>
            <span>Users Management</span>
        </a>
        <div id="collapseUsersManagement" class="collapse" aria-labelledby="headingThree"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('users.index') }}">User</a>
                <a class="collapse-item" href="#">Role</a>
                <a class="collapse-item" href="#">Permission</a>
            </div>
        </div>
    </li>
</ul>
<!-- End of Sidebar -->
